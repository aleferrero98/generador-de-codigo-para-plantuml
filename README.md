# Generador de código para PlantUML

#### Proyecto Final de la asignatura *"Práctica y construcción de compiladores"* - FCEFyN - UNC.

**Versión:** 1.0.0

**Autor:** Alejandro Ferrero

**Email:** ale.ferrero@mi.unc.edu.ar
## Descripción
Consiste en un generador de código Java a partir de diagramas de clases desarrollados en *PlantUML*. Se realiza un análisis léxico, sintáctico y semántico del diagrama de entrada previo a la generación de los archivos .java. Se notifican posibles errores y/o warnings.


## Pasos de ejecución
1. Ejecutar archivo con extensión .jar
2. En la interfaz gráfica, seleccionar la ruta al archivo de entrada que contiene el diagrama de clases UML, el cual deberá implementarse con la sintaxis indicada por **PlantUML**.
3. Seleccionar directorio de destino en donde se guardarán los archivos generados.
4. Generar código.
5. Si no se presentan errores, el código generado estará disponible en el directorio destino indicado en el *paso 3*.
