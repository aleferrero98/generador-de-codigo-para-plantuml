// Generated from /home/alejandro/Escritorio/FACULTAD/Practica_y_construccion_de_compiladores/TP_final/generador-de-codigo-para-plantuml/GeneradorDeCodigo/src/GeneradorCodigo.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GeneradorCodigoParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GeneradorCodigoVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#inicio}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInicio(GeneradorCodigoParser.InicioContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#diagrama_uml}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDiagrama_uml(GeneradorCodigoParser.Diagrama_umlContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#modificadores}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModificadores(GeneradorCodigoParser.ModificadoresContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#comando_skinparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComando_skinparam(GeneradorCodigoParser.Comando_skinparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#objetos_a_esconder}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjetos_a_esconder(GeneradorCodigoParser.Objetos_a_esconderContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#declaracion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracion(GeneradorCodigoParser.DeclaracionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#coloreado}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColoreado(GeneradorCodigoParser.ColoreadoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#tipo_elemento}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipo_elemento(GeneradorCodigoParser.Tipo_elementoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#bloque}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloque(GeneradorCodigoParser.BloqueContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#contenido_bloque}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContenido_bloque(GeneradorCodigoParser.Contenido_bloqueContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#atributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtributo(GeneradorCodigoParser.AtributoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#alcance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlcance(GeneradorCodigoParser.AlcanceContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#tipo_dato}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipo_dato(GeneradorCodigoParser.Tipo_datoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#estatico}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEstatico(GeneradorCodigoParser.EstaticoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#c_final}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitC_final(GeneradorCodigoParser.C_finalContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#metodo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMetodo(GeneradorCodigoParser.MetodoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#abstracto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbstracto(GeneradorCodigoParser.AbstractoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#valor_retorno}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValor_retorno(GeneradorCodigoParser.Valor_retornoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#prototipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrototipo(GeneradorCodigoParser.PrototipoContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#nombre_funcion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNombre_funcion(GeneradorCodigoParser.Nombre_funcionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#argumentos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgumentos(GeneradorCodigoParser.ArgumentosContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#varios_argumentos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarios_argumentos(GeneradorCodigoParser.Varios_argumentosContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#relacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelacion(GeneradorCodigoParser.RelacionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GeneradorCodigoParser#flecha}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFlecha(GeneradorCodigoParser.FlechaContext ctx);
}