package tabla_simbolos;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase Identificador que representa cada elemento (clase, clase abstracta, enum, interface) que 
 * se define en el diagrama y se guarda en la tabla de simbolos.
 * Contiene toda la informacion del elemento, como su nombre, tipo, metodos, atributos, interfaces que implementa, etc.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class Identificador {
    //Atributos
    private String id;
    private String tipoElemento; //tipo puede ser class, enum, interface o abstract class
    private Boolean usada; //es usada si aparece en alguna relacion
    private Identificador clasePadre;
    private List<Identificador> interfaces;
    private List<Miembro> atributos;
    private List<Miembro> metodos;

    /**
     * Constructor que crea un Identificador con solo saber su nombre y tipo.
     * @param id nombre identificador del elemento.
     * @param tipoElemento tipo de dato del elemento (class, enum, interface, abstract class)
     */
    public Identificador(String id, String tipoElemento){
        this.id = id;
        this.tipoElemento = tipoElemento;
        this.usada = false;
        //si no hereda de ninguna clase -> clasePadre es null
        this.clasePadre = null;
        //en caso de que no implemente ninguna interfaz -> la lista interfaces esta vacia.
        this.interfaces = new ArrayList<Identificador>();
        //si no tiene atributos o metodos las listas estaran vacias
        this.atributos = new ArrayList<Miembro>();
        this.metodos = new ArrayList<Miembro>();
    }

    /**
     * Constructor que se utiliza para elementos que contienen atributos y/o metodos definidos
     * @param id nombre del elemento
     * @param tipoElemento tipo de dato
     * @param atributos lista de atributos que contiene
     * @param metodos lista de metodos que contiene
     */
    public Identificador(String id, String tipoElemento, List<Miembro> atributos, List<Miembro> metodos){
        this.id = id;
        this.tipoElemento = tipoElemento;
        this.usada = false;
        //si no hereda de ninguna clase -> clasePadre es null
        this.clasePadre = null;
        //en caso de que no implemente ninguna interfaz -> la lista interfaces esta vacia.
        this.interfaces = new ArrayList<Identificador>();
        //si no tiene atributos o metodos las listas estaran vacias
        this.atributos = atributos;
        this.metodos = metodos;
    }
//--------------------------------------GET-------------------------------------
    /**
     * Obtiene el nombre del Identificador
     * @return string con el nombre del Identificador.
     */
    public String getID(){
        return id;
    }
    /**
     * Obtiene el tipo del Identificador
     * @return string con el tipo del Identificador.
     */
    public String getTipoElemento(){
        return tipoElemento;
    }
    /**
     * Indica si el elemento fue usado, es decir, si aparece relacionado con otro elemento. 
     * @return true si aparece en algun lado de una relacion, false en caso contrario.
     */
    public Boolean getUsada(){
        return usada;
    }
    /**
     * Obtiene el Identificador que representa la clase padre del elemento actual.
     * @return el identificador de la clase padre o null si dicho elemento no hereda de ninguna clase.
     */
    public Identificador getClasePadre(){
        return clasePadre;
    }
    /**
     * Obtiene la lista de interfaces que implementa el Identificador actual.
     * @return la lista de interfaces (estará vacia si no implementa ninguna interfaz).
     */
    public List<Identificador> getInterfaces(){
        return interfaces;
    }
    /**
     * Obtiene la lista de atributos del Identificador
     * @return la lista de atributos (son Miembros), estará vacia si no se especificaron atributos.
     */
    public List<Miembro> getAtributos(){
        return atributos;
    }
    /**
     * Devuelve la lista de metodos del Identificador
     * @return la lista de metodos (son Miembros), estará vacia si no implementa ningun metodo.
     */
    public List<Miembro> getMetodos(){
        return metodos;
    }
//-----------------------------------SET-------------------------------------
    /**
     * Setea el atributo usada.
     * @param usada es true o false para indicar si el Identificador fue usado o no en alguna relacion.
     */
    public void setUsada(Boolean usada){
        this.usada = usada;
    }
    /**
     * Establece la clase padre de la que hereda el Identificador actual.
     * @param padre Identificador que representa a la clase padre
     */
    public void setClasePadre(Identificador padre){
        this.clasePadre = padre;
    }
    /**
     * Establece la lista de interfaces que implementa el Identificador
     * @param interfaces lista de Identificadores, uno por cada interfaz que implemente.
     */
    public void setInterfaces(ArrayList<Identificador> interfaces){
        this.interfaces = interfaces;
    }
    /**
     * Establece la lista de atributos que posee el Identificador
     * @param atributos lista de atributos.
     */
    public void setAtributos(ArrayList<Miembro> atributos){
        this.atributos = atributos;
    }
    /**
     * Establece la lista de metodos que implementa el Identificador
     * @param metodos lista de metodos.
     */
    public void setMetodos(ArrayList<Miembro> metodos){
        this.metodos = metodos;
    }
//-------------------------------------------------------------------------------
    /**
     * Agrega una interfaz a la lista de interfaces que implementa el Identificador
     * @param interfaz interfaz a agregar.
     */
    public void agregarInterfaz(Identificador interfaz){
        this.interfaces.add(interfaz);
    }
    /**
     * Agrega un atributo del identificador(this) a la lista de atributos.
     * @param atributo atributo a agregar.
     */
    public void agregarAtributo(Atributo atributo){
        this.atributos.add(atributo);
    }
    /**
     * Agrega un metodo del identificador(this) a la lista de metodos.
     * @param metodo metodo a agregar.
     */
    public void agregarMetodo(Metodo metodo){
        this.metodos.add(metodo);
    }
    /**
     * Recorre la lista de interfaces que implementa el identificador y obtiene
     * el nombre de cada una.
     * @param lista lista de interfaces a recorrer.
     * @return string con el nombre de todas las interfaces.
     */
    public String recorrerListaInterfaces(List<Identificador> lista){
        if(lista.isEmpty()){
            return "-";
        }else{
            String texto = "";
            for(Identificador elem : lista){
                texto += elem.getID();
                texto += ", ";
            }
            return texto;
        }
    }
    /**
     * Recorre la lista de miembros (metodos o atributos) y obtiene solo el nombre o id de cada uno.
     * @param lista lista a recorrer.
     * @return string con el nombre de todos los miembros, separados por comas.
     */
    public String getIdListaMiembro(List<Miembro> lista){
        if(lista.isEmpty()){
            return "-";
        }else{
            String texto = "";
            for(Miembro elem : lista){
                texto += elem.getID();
                texto += ", ";
            }
            return texto;
        }
    }
    /**
     * Recorre la lista de miembros (metodos o atributos) y obtiene toda la informacion de cada uno.
     * @param lista lista a recorrer.
     * @return string con la informacion de todas los miembros.
     */
    public String getInfoListaMiembro(List<Miembro> lista){
        if(lista.isEmpty()){
            return "";
        }else{
            String texto = "";
            for(Miembro elem : lista){
                texto += "\t|\n\t+->" + elem.toString();
            }
            texto += "\n";
            return texto;
        }
    }
    /**
     * Genera una representacion del Identificador con todos sus datos.
     * @return string con toda la informacion del Identificador.
     */
    public String toString(){
        String salida;
        String padre = ((clasePadre == null)? "-" : clasePadre.getID());
        String descripAtributos = getInfoListaMiembro(this.atributos);
        String descripMetodos = getInfoListaMiembro(this.metodos);

        salida = "ID: " + this.id + "\n" 
               + "-> tipo de elemento: " + this.tipoElemento + "\n" 
               + "-> usada: " + this.usada + "\n" 
               + "-> clase padre: " + padre + "\n"
               + "-> interfaces: " + recorrerListaInterfaces(interfaces) + "\n"
               + "-> atributos: " + getIdListaMiembro(this.atributos) + "\n"
               + descripAtributos
               + "-> metodos: " + getIdListaMiembro(this.metodos) + "\n"
               + descripMetodos;

        return salida;
    }
    
}
