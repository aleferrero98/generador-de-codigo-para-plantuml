package tabla_simbolos;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase Metodo que implementa el metodo abstracto toString de la clase Miembro.
 * Representa un método o funcion con sus argumentos y tipo de dato devuelto. 
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class Metodo extends Miembro {
    private List<Argumento> argumentos;
    private Boolean isAbstract;

    /**
     * Constructor que inicializa los datos de un metodo.
     * @param id nombre del metodo
     * @param tipoDevuelto tipo de dato devuelto
     * @param visibilidad alcance (public, private o protected)
     * @param isStatic si es un metodo static o no
     * @param isFinal si es metodo final o no 
     * @param isAbstract si es un metodo abstract o no
     */
    public Metodo(String id, String tipoDevuelto, String visibilidad, Boolean isStatic, Boolean isFinal, Boolean isAbstract){
        super(id, tipoDevuelto, visibilidad, isStatic, isFinal);
        this.isAbstract = isAbstract;
        //si la funcion no tiene argumentos, la lista estará vacia
        argumentos = new ArrayList<Argumento>();
    }
    /**
     * Constructor que iniciliza tambien la lista de argumentos del metodo.
     * @param id nombre del metodo
     * @param tipoDevuelto tipo de dato devuelto
     * @param visibilidad alcance (public, private o protected)
     * @param isStatic si es un metodo static o no
     * @param isFinal si es metodo final o no 
     * @param isAbstract si es un metodo abstract o no
     * @param argumentos lista de argumentos del metodo
     */
    public Metodo(String id, String tipoDevuelto, String visibilidad, Boolean isStatic, Boolean isFinal, Boolean isAbstract, List<Argumento> argumentos){
        super(id, tipoDevuelto, visibilidad, isStatic, isFinal);
        this.isAbstract = isAbstract;
        this.argumentos = argumentos;
    }
    /**
     * Obtiene el flag que indica si es un metodo abstracto.
     * @return true si es abstracto, false en caso contrario.
     */
    public Boolean getIsAbstract(){
        return this.isAbstract;
    }
    /**
     * Obtiene la lista de argumentos del metodo
     * @return lista de argumentos
     */
    public List<Argumento> getArgumentos(){
        return this.argumentos;
    }
    /**
     * Agrega de a un argumento a la lista.
     * @param arg argumento a agregar
     */
    public void agregarArgumento(Argumento arg){
        this.argumentos.add(arg);
    }

    @Override
    public String toString(){
        String salida;
        salida = "ID: " + this.getID() + "\n" 
               + "\t -> tipo devuelto: " + this.getTipo() + "\n" 
               + "\t -> visibilidad: " + this.getVisibilidad() + "\n" 
               + "\t -> es static: " + this.getIsStatic() + "\n"
               + "\t -> es final: " + this.getIsFinal() + "\n"
               + "\t -> es abstract: " + this.getIsAbstract() + "\n"
               + "\t -> argumentos: " + this.getArgumentos() + "\n";

        return salida;
    }
    
}
