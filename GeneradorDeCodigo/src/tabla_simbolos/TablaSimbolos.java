package tabla_simbolos;

import java.util.*;
import java.lang.String;

/**
 * Clase Tabla de Simbolos que contiene informacion de todos los elementos 
 * (clases, interfaces, enum, clases abstractas) usados en el diagrama UML.
 * Los elementos son identificadores y se encuentran en un Map, donde la clave es el nombre.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class TablaSimbolos {
    private Map<String,Identificador> tabla; //hashmap donde la clave es el nombre del Identificador.
    private static TablaSimbolos tablaSimbolos;

    /**
     * Contructor privado para tener una sola instancia de la TablaSimbolos, inicializa la tabla.
     */
    private TablaSimbolos(){
        this.tabla = new HashMap<String,Identificador>();
    }

    /**
     * Metodo que crea una única instancia de la tabla de simbolos en la primera vez que es llamado, 
     * y devuelve la Tabla de Simbolos creada en las llamadas siguientes (patron Singleton).
     * @return la Tabla de Simbolos
     */
    public static TablaSimbolos getInstanceOf(){
        if(tablaSimbolos == null){
            tablaSimbolos = new TablaSimbolos();
        }
        return tablaSimbolos;
    }
    /**
     * Agrega un elemento(clase, enum, interface o clase abstracta) a la tabla de simbolos.
     * @param id identificador a agregar.
     */
    public void agregarID(Identificador id){
        this.tabla.put(id.getID(), id);
    }
    /**
     * Busca un identificador en la tabla de simbolos.
     * @param nombre nombre del identificador a buscar (clave del Map)
     * @return el objeto Identificador o null si no se encuentra.
     */
    public Identificador buscarID(String nombre){
        return tabla.get(nombre);
    }

    /**
     * Controla si el nombre del elemento(Identificador) ya es utilizado por otro elemento en la tabla de simbolos.
     * @param nombre nombre a verificar su existencia.
     * @return true si el ID ya esta siendo utilizado, falso en caso contrario.
     */
    public Boolean idExist(String nombre){
        if(this.tabla.get(nombre) == null){
            return false;
        }else return true;
    }

    /**
     * Getter que devuelve el Map que representa la tabla de simbolos.
     * @return Map que contiene los elementos de la tabla de simbolos.
     */
    public Map<String,Identificador> getTablaSimbolos(){
        return this.tabla;
    }

    /**
     * Genera un string con todos los Identificadores capturados en la tabla de simbolos.
     * @return representacion del texto a imprimir.
     */
    public String toString(){
        String texto = "--------------------TABLA DE SIMBOLOS--------------------\n";
        //recorro todos los valores (Identificadores) encontrados
        for(Identificador item : tabla.values()) {
            texto += item.toString();
            texto += "---------------------------------------------------------\n";
        }
        return texto;
    }

}