package tabla_simbolos;

/**
 * Clase Atributo que representa los atributos de una clase.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class Atributo extends Miembro {
    /**
     * Constructor que inicializa la informacion de un atributo.
     * @param id nombre del atributo
     * @param tipoDato tipo de dato
     * @param visibilidad alcance (public, private o protected)
     * @param isStatic flag que indica si es static
     * @param isFinal flag que indica si es final (constante)
     */
    public Atributo(String id, String tipoDato, String visibilidad, Boolean isStatic, Boolean isFinal){
        super(id, tipoDato, visibilidad, isStatic, isFinal);
    }

    @Override
    public String toString(){
        String salida;

        salida = "ID: " + this.getID() + "\n" 
               + "\t -> tipo de dato: " + this.getTipo() + "\n" 
               + "\t -> visibilidad: " + this.getVisibilidad() + "\n" 
               + "\t -> es static: " + this.getIsStatic() + "\n"
               + "\t -> es final: " + this.getIsFinal() + "\n";

        return salida;
    }
    
}
