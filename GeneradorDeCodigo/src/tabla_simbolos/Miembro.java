package tabla_simbolos;

/**
 * Clase abstracta Miembro que define el supertipo que abarca tanto a Metodos como Atributos.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public abstract class Miembro {
    private String id;
    private String tipoDato; //tipo de dato devuelto en una funcion o tipo de dato de un atributo
    private String visibilidad;
    private Boolean isStatic;
    private Boolean isFinal;

    /**
     * Constructor que inicializa los datos de un Objeto Miembro (metodo o atributo)
     * @param id nombre del atributo/metodo
     * @param tipo tipo de dato del atributo o tipo de dato devuelto por un metodo
     * @param alcance indica si es publico, privado o protegido
     * @param esEstatico flag que indica si es static (metodo o atributo)
     * @param esFinal flag que indica si es final (metodo o atributo)
     */
    public Miembro(String id, String tipo, String alcance, Boolean esEstatico, Boolean esFinal){
        this.id = id;
        this.tipoDato = tipo;
        this.visibilidad = alcance;
        this.isStatic = esEstatico;
        this.isFinal = esFinal;
    }
    /**
     * Obtiene el nombre del metodo/atributo.
     * @return string con el identificador.
     */
    public String getID(){
        return this.id;
    }
    /**
     * Obtiene el tipo de dato devuelto por un metodo o el tipo de dato de un atributo.
     * @return string con el tipo de dato.
     */
    public String getTipo(){
        return this.tipoDato;
    }
    /**
     * Devuelve el alcance o nivel de visibilidad de un metodo/atributo.
     * @return string con public, private o protected.
     */
    public String getVisibilidad(){
        return this.visibilidad;
    }
    /**
     * Obtiene el flag que indica si es static
     * @return true si es static, false en caso contrario.
     */
    public Boolean getIsStatic(){
        return this.isStatic;
    }
    /**
     * Obtiene el flag que indica si es final.
     * @return true si es final, flase en caso contrario.
     */
    public Boolean getIsFinal(){
        return this.isFinal;
    }
    /**
     * Genera un string con toda la informacion del metodo/atributo.
     * @return string con la representacion del miembro.
     */
    public abstract String toString(); 

}
