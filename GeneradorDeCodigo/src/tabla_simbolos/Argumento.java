package tabla_simbolos;

/**
 * Clase Argumento que representa los argumentos o parametros de un metodo (con su nombre y tipo de dato).
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class Argumento {
    private String id;
    private String tipoDato;

    /**
     * Constructor que inicializa los datos que identifican un argumento dentro de un metodo.
     * Los argumentos podrian definirse solo con el tipo de dato, pero al poner su id se evitan posibles errores de tipeo.
     * @param id nombre del argumento
     * @param tipo tipo de dato
     */
    public Argumento(String id, String tipo){
        this.id = id;
        this.tipoDato = tipo;
    }
    /**
     * Obtiene el id del argumento
     * @return string con el id
     */
    public String getID(){
        return this.id;
    }
    /**
     * Obtiene el tipo de dato del argumento
     * @return string con el tipo de dato
     */
    public String getTipo(){
        return this.tipoDato;
    }
    /**
     * Genera una representacion del argumento
     * @return string con la infromacion del argumento
     */
    public String toString(){
        String texto;
        texto = this.id + " (" + this.tipoDato + ")";
        return texto;
    }
}
