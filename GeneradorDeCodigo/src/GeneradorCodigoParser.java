// Generated from /home/alejandro/Escritorio/FACULTAD/Practica_y_construccion_de_compiladores/TP_final/generador-de-codigo-para-plantuml/GeneradorDeCodigo/src/GeneradorCodigo.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GeneradorCodigoParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LA=1, LC=2, PA=3, PC=4, COMA=5, GUION_BAJO=6, COM_ANG_IZQ=7, COM_ANG_DER=8, 
		DOS_PUNTOS=9, CLASE=10, CLASE_ABSTRACTA=11, ENUM=12, INTERFACE=13, ABSTRACTA=14, 
		ESTATICO=15, FINAL=16, ESCONDER=17, MOSTRAR=18, SKINPARAM=19, PUBLIC=20, 
		PRIVATE=21, PROTECTED=22, IZQ=23, DER=24, ARRIBA=25, ABAJO=26, NOTA=27, 
		COMO=28, MIEMBROS_VACIOS=29, ATRIBUTOS_VACIOS=30, METODOS_VACIOS=31, ATRIBUTOS=32, 
		METODOS=33, MIEMBROS=34, CIRCULO=35, STEREOTYPE=36, END=37, POSICION=38, 
		HEREDA_DE=39, ES_PADRE_DE=40, DEPENDE_DE=41, ES_USADA_POR=42, IMPLEMENTA=43, 
		ES_IMPLEMENTADA_POR=44, ASOCIACION=45, SE_ASOCIA_A=46, ES_ASOCIADO_CON=47, 
		NUMERO=48, ID=49, ESTEREOTIPO=50, COLOR=51, START=52, WS=53, COMENTARIO=54, 
		SEPARADOR=55;
	public static final int
		RULE_inicio = 0, RULE_diagrama_uml = 1, RULE_modificadores = 2, RULE_comando_skinparam = 3, 
		RULE_objetos_a_esconder = 4, RULE_declaracion = 5, RULE_coloreado = 6, 
		RULE_tipo_elemento = 7, RULE_bloque = 8, RULE_contenido_bloque = 9, RULE_atributo = 10, 
		RULE_alcance = 11, RULE_tipo_dato = 12, RULE_estatico = 13, RULE_c_final = 14, 
		RULE_metodo = 15, RULE_abstracto = 16, RULE_valor_retorno = 17, RULE_prototipo = 18, 
		RULE_nombre_funcion = 19, RULE_argumentos = 20, RULE_varios_argumentos = 21, 
		RULE_relacion = 22, RULE_flecha = 23;
	private static String[] makeRuleNames() {
		return new String[] {
			"inicio", "diagrama_uml", "modificadores", "comando_skinparam", "objetos_a_esconder", 
			"declaracion", "coloreado", "tipo_elemento", "bloque", "contenido_bloque", 
			"atributo", "alcance", "tipo_dato", "estatico", "c_final", "metodo", 
			"abstracto", "valor_retorno", "prototipo", "nombre_funcion", "argumentos", 
			"varios_argumentos", "relacion", "flecha"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'{'", "'}'", "'('", "')'", "','", "'_'", "'<<'", "'>>'", "':'", 
			"'class'", "'abstract class'", "'enum'", "'interface'", "'abstract'", 
			"'static'", "'final'", "'hide'", "'show'", "'skinparam'", "'+'", "'-'", 
			"'#'", "'left'", "'right'", "'up'", "'down'", "'note'", "'as'", "'empty members'", 
			null, "'empty methods'", null, "'methods'", "'members'", "'circle'", 
			"'stereotype'", "'@enduml'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "LA", "LC", "PA", "PC", "COMA", "GUION_BAJO", "COM_ANG_IZQ", "COM_ANG_DER", 
			"DOS_PUNTOS", "CLASE", "CLASE_ABSTRACTA", "ENUM", "INTERFACE", "ABSTRACTA", 
			"ESTATICO", "FINAL", "ESCONDER", "MOSTRAR", "SKINPARAM", "PUBLIC", "PRIVATE", 
			"PROTECTED", "IZQ", "DER", "ARRIBA", "ABAJO", "NOTA", "COMO", "MIEMBROS_VACIOS", 
			"ATRIBUTOS_VACIOS", "METODOS_VACIOS", "ATRIBUTOS", "METODOS", "MIEMBROS", 
			"CIRCULO", "STEREOTYPE", "END", "POSICION", "HEREDA_DE", "ES_PADRE_DE", 
			"DEPENDE_DE", "ES_USADA_POR", "IMPLEMENTA", "ES_IMPLEMENTADA_POR", "ASOCIACION", 
			"SE_ASOCIA_A", "ES_ASOCIADO_CON", "NUMERO", "ID", "ESTEREOTIPO", "COLOR", 
			"START", "WS", "COMENTARIO", "SEPARADOR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "GeneradorCodigo.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GeneradorCodigoParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class InicioContext extends ParserRuleContext {
		public TerminalNode START() { return getToken(GeneradorCodigoParser.START, 0); }
		public Diagrama_umlContext diagrama_uml() {
			return getRuleContext(Diagrama_umlContext.class,0);
		}
		public TerminalNode END() { return getToken(GeneradorCodigoParser.END, 0); }
		public TerminalNode EOF() { return getToken(GeneradorCodigoParser.EOF, 0); }
		public InicioContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inicio; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterInicio(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitInicio(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitInicio(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InicioContext inicio() throws RecognitionException {
		InicioContext _localctx = new InicioContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_inicio);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			match(START);
			setState(49);
			diagrama_uml();
			setState(50);
			match(END);
			setState(51);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Diagrama_umlContext extends ParserRuleContext {
		public DeclaracionContext declaracion() {
			return getRuleContext(DeclaracionContext.class,0);
		}
		public Diagrama_umlContext diagrama_uml() {
			return getRuleContext(Diagrama_umlContext.class,0);
		}
		public RelacionContext relacion() {
			return getRuleContext(RelacionContext.class,0);
		}
		public ModificadoresContext modificadores() {
			return getRuleContext(ModificadoresContext.class,0);
		}
		public Diagrama_umlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_diagrama_uml; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterDiagrama_uml(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitDiagrama_uml(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitDiagrama_uml(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Diagrama_umlContext diagrama_uml() throws RecognitionException {
		Diagrama_umlContext _localctx = new Diagrama_umlContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_diagrama_uml);
		try {
			setState(63);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CLASE:
			case CLASE_ABSTRACTA:
			case ENUM:
			case INTERFACE:
			case ABSTRACTA:
				enterOuterAlt(_localctx, 1);
				{
				setState(53);
				declaracion();
				setState(54);
				diagrama_uml();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(56);
				relacion();
				setState(57);
				diagrama_uml();
				}
				break;
			case ESCONDER:
			case MOSTRAR:
			case SKINPARAM:
				enterOuterAlt(_localctx, 3);
				{
				setState(59);
				modificadores();
				setState(60);
				diagrama_uml();
				}
				break;
			case END:
				enterOuterAlt(_localctx, 4);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModificadoresContext extends ParserRuleContext {
		public TerminalNode ESCONDER() { return getToken(GeneradorCodigoParser.ESCONDER, 0); }
		public Objetos_a_esconderContext objetos_a_esconder() {
			return getRuleContext(Objetos_a_esconderContext.class,0);
		}
		public TerminalNode MOSTRAR() { return getToken(GeneradorCodigoParser.MOSTRAR, 0); }
		public Comando_skinparamContext comando_skinparam() {
			return getRuleContext(Comando_skinparamContext.class,0);
		}
		public ModificadoresContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modificadores; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterModificadores(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitModificadores(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitModificadores(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModificadoresContext modificadores() throws RecognitionException {
		ModificadoresContext _localctx = new ModificadoresContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_modificadores);
		try {
			setState(70);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ESCONDER:
				enterOuterAlt(_localctx, 1);
				{
				setState(65);
				match(ESCONDER);
				setState(66);
				objetos_a_esconder();
				}
				break;
			case MOSTRAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(67);
				match(MOSTRAR);
				setState(68);
				objetos_a_esconder();
				}
				break;
			case SKINPARAM:
				enterOuterAlt(_localctx, 3);
				{
				setState(69);
				comando_skinparam();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Comando_skinparamContext extends ParserRuleContext {
		public TerminalNode SKINPARAM() { return getToken(GeneradorCodigoParser.SKINPARAM, 0); }
		public List<TerminalNode> ID() { return getTokens(GeneradorCodigoParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(GeneradorCodigoParser.ID, i);
		}
		public TerminalNode NUMERO() { return getToken(GeneradorCodigoParser.NUMERO, 0); }
		public Comando_skinparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comando_skinparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterComando_skinparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitComando_skinparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitComando_skinparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Comando_skinparamContext comando_skinparam() throws RecognitionException {
		Comando_skinparamContext _localctx = new Comando_skinparamContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_comando_skinparam);
		try {
			setState(78);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(72);
				match(SKINPARAM);
				setState(73);
				match(ID);
				setState(74);
				match(NUMERO);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(75);
				match(SKINPARAM);
				setState(76);
				match(ID);
				setState(77);
				match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Objetos_a_esconderContext extends ParserRuleContext {
		public TerminalNode MIEMBROS_VACIOS() { return getToken(GeneradorCodigoParser.MIEMBROS_VACIOS, 0); }
		public TerminalNode ATRIBUTOS_VACIOS() { return getToken(GeneradorCodigoParser.ATRIBUTOS_VACIOS, 0); }
		public TerminalNode METODOS_VACIOS() { return getToken(GeneradorCodigoParser.METODOS_VACIOS, 0); }
		public TerminalNode ATRIBUTOS() { return getToken(GeneradorCodigoParser.ATRIBUTOS, 0); }
		public TerminalNode METODOS() { return getToken(GeneradorCodigoParser.METODOS, 0); }
		public TerminalNode MIEMBROS() { return getToken(GeneradorCodigoParser.MIEMBROS, 0); }
		public TerminalNode CIRCULO() { return getToken(GeneradorCodigoParser.CIRCULO, 0); }
		public TerminalNode STEREOTYPE() { return getToken(GeneradorCodigoParser.STEREOTYPE, 0); }
		public TerminalNode CLASE() { return getToken(GeneradorCodigoParser.CLASE, 0); }
		public TerminalNode INTERFACE() { return getToken(GeneradorCodigoParser.INTERFACE, 0); }
		public TerminalNode ENUM() { return getToken(GeneradorCodigoParser.ENUM, 0); }
		public TerminalNode ESTEREOTIPO() { return getToken(GeneradorCodigoParser.ESTEREOTIPO, 0); }
		public TerminalNode ID() { return getToken(GeneradorCodigoParser.ID, 0); }
		public Objetos_a_esconderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objetos_a_esconder; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterObjetos_a_esconder(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitObjetos_a_esconder(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitObjetos_a_esconder(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Objetos_a_esconderContext objetos_a_esconder() throws RecognitionException {
		Objetos_a_esconderContext _localctx = new Objetos_a_esconderContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_objetos_a_esconder);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CLASE) | (1L << ENUM) | (1L << INTERFACE) | (1L << MIEMBROS_VACIOS) | (1L << ATRIBUTOS_VACIOS) | (1L << METODOS_VACIOS) | (1L << ATRIBUTOS) | (1L << METODOS) | (1L << MIEMBROS) | (1L << CIRCULO) | (1L << STEREOTYPE) | (1L << ID) | (1L << ESTEREOTIPO))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaracionContext extends ParserRuleContext {
		public Tipo_elementoContext tipo_elemento() {
			return getRuleContext(Tipo_elementoContext.class,0);
		}
		public TerminalNode ID() { return getToken(GeneradorCodigoParser.ID, 0); }
		public TerminalNode ESTEREOTIPO() { return getToken(GeneradorCodigoParser.ESTEREOTIPO, 0); }
		public ColoreadoContext coloreado() {
			return getRuleContext(ColoreadoContext.class,0);
		}
		public BloqueContext bloque() {
			return getRuleContext(BloqueContext.class,0);
		}
		public DeclaracionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaracion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterDeclaracion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitDeclaracion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitDeclaracion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclaracionContext declaracion() throws RecognitionException {
		DeclaracionContext _localctx = new DeclaracionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_declaracion);
		try {
			setState(93);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(82);
				tipo_elemento();
				setState(83);
				match(ID);
				setState(84);
				match(ESTEREOTIPO);
				setState(85);
				coloreado();
				setState(86);
				bloque();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(88);
				tipo_elemento();
				setState(89);
				match(ID);
				setState(90);
				coloreado();
				setState(91);
				bloque();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColoreadoContext extends ParserRuleContext {
		public TerminalNode COLOR() { return getToken(GeneradorCodigoParser.COLOR, 0); }
		public ColoreadoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_coloreado; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterColoreado(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitColoreado(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitColoreado(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColoreadoContext coloreado() throws RecognitionException {
		ColoreadoContext _localctx = new ColoreadoContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_coloreado);
		try {
			setState(97);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COLOR:
				enterOuterAlt(_localctx, 1);
				{
				setState(95);
				match(COLOR);
				}
				break;
			case LA:
			case CLASE:
			case CLASE_ABSTRACTA:
			case ENUM:
			case INTERFACE:
			case ABSTRACTA:
			case ESCONDER:
			case MOSTRAR:
			case SKINPARAM:
			case END:
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Tipo_elementoContext extends ParserRuleContext {
		public TerminalNode ENUM() { return getToken(GeneradorCodigoParser.ENUM, 0); }
		public TerminalNode CLASE() { return getToken(GeneradorCodigoParser.CLASE, 0); }
		public TerminalNode INTERFACE() { return getToken(GeneradorCodigoParser.INTERFACE, 0); }
		public TerminalNode CLASE_ABSTRACTA() { return getToken(GeneradorCodigoParser.CLASE_ABSTRACTA, 0); }
		public TerminalNode ABSTRACTA() { return getToken(GeneradorCodigoParser.ABSTRACTA, 0); }
		public Tipo_elementoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tipo_elemento; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterTipo_elemento(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitTipo_elemento(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitTipo_elemento(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tipo_elementoContext tipo_elemento() throws RecognitionException {
		Tipo_elementoContext _localctx = new Tipo_elementoContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_tipo_elemento);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CLASE) | (1L << CLASE_ABSTRACTA) | (1L << ENUM) | (1L << INTERFACE) | (1L << ABSTRACTA))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueContext extends ParserRuleContext {
		public TerminalNode LA() { return getToken(GeneradorCodigoParser.LA, 0); }
		public Contenido_bloqueContext contenido_bloque() {
			return getRuleContext(Contenido_bloqueContext.class,0);
		}
		public TerminalNode LC() { return getToken(GeneradorCodigoParser.LC, 0); }
		public BloqueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloque; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterBloque(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitBloque(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitBloque(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueContext bloque() throws RecognitionException {
		BloqueContext _localctx = new BloqueContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_bloque);
		try {
			setState(106);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LA:
				enterOuterAlt(_localctx, 1);
				{
				setState(101);
				match(LA);
				setState(102);
				contenido_bloque();
				setState(103);
				match(LC);
				}
				break;
			case CLASE:
			case CLASE_ABSTRACTA:
			case ENUM:
			case INTERFACE:
			case ABSTRACTA:
			case ESCONDER:
			case MOSTRAR:
			case SKINPARAM:
			case END:
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Contenido_bloqueContext extends ParserRuleContext {
		public AtributoContext atributo() {
			return getRuleContext(AtributoContext.class,0);
		}
		public Contenido_bloqueContext contenido_bloque() {
			return getRuleContext(Contenido_bloqueContext.class,0);
		}
		public MetodoContext metodo() {
			return getRuleContext(MetodoContext.class,0);
		}
		public Contenido_bloqueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contenido_bloque; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterContenido_bloque(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitContenido_bloque(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitContenido_bloque(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Contenido_bloqueContext contenido_bloque() throws RecognitionException {
		Contenido_bloqueContext _localctx = new Contenido_bloqueContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_contenido_bloque);
		try {
			setState(115);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(108);
				atributo();
				setState(109);
				contenido_bloque();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(111);
				metodo();
				setState(112);
				contenido_bloque();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtributoContext extends ParserRuleContext {
		public AlcanceContext alcance() {
			return getRuleContext(AlcanceContext.class,0);
		}
		public EstaticoContext estatico() {
			return getRuleContext(EstaticoContext.class,0);
		}
		public C_finalContext c_final() {
			return getRuleContext(C_finalContext.class,0);
		}
		public Tipo_datoContext tipo_dato() {
			return getRuleContext(Tipo_datoContext.class,0);
		}
		public TerminalNode ID() { return getToken(GeneradorCodigoParser.ID, 0); }
		public AtributoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atributo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterAtributo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitAtributo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitAtributo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtributoContext atributo() throws RecognitionException {
		AtributoContext _localctx = new AtributoContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_atributo);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			alcance();
			setState(118);
			estatico();
			setState(119);
			c_final();
			setState(120);
			tipo_dato();
			setState(121);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AlcanceContext extends ParserRuleContext {
		public TerminalNode PUBLIC() { return getToken(GeneradorCodigoParser.PUBLIC, 0); }
		public TerminalNode PRIVATE() { return getToken(GeneradorCodigoParser.PRIVATE, 0); }
		public TerminalNode PROTECTED() { return getToken(GeneradorCodigoParser.PROTECTED, 0); }
		public AlcanceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alcance; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterAlcance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitAlcance(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitAlcance(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AlcanceContext alcance() throws RecognitionException {
		AlcanceContext _localctx = new AlcanceContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_alcance);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PUBLIC) | (1L << PRIVATE) | (1L << PROTECTED))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Tipo_datoContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GeneradorCodigoParser.ID, 0); }
		public Tipo_datoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tipo_dato; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterTipo_dato(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitTipo_dato(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitTipo_dato(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tipo_datoContext tipo_dato() throws RecognitionException {
		Tipo_datoContext _localctx = new Tipo_datoContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_tipo_dato);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EstaticoContext extends ParserRuleContext {
		public TerminalNode LA() { return getToken(GeneradorCodigoParser.LA, 0); }
		public TerminalNode ESTATICO() { return getToken(GeneradorCodigoParser.ESTATICO, 0); }
		public TerminalNode LC() { return getToken(GeneradorCodigoParser.LC, 0); }
		public EstaticoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_estatico; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterEstatico(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitEstatico(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitEstatico(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EstaticoContext estatico() throws RecognitionException {
		EstaticoContext _localctx = new EstaticoContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_estatico);
		try {
			setState(131);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LA:
				enterOuterAlt(_localctx, 1);
				{
				setState(127);
				match(LA);
				setState(128);
				match(ESTATICO);
				setState(129);
				match(LC);
				}
				break;
			case FINAL:
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class C_finalContext extends ParserRuleContext {
		public TerminalNode FINAL() { return getToken(GeneradorCodigoParser.FINAL, 0); }
		public C_finalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_c_final; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterC_final(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitC_final(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitC_final(this);
			else return visitor.visitChildren(this);
		}
	}

	public final C_finalContext c_final() throws RecognitionException {
		C_finalContext _localctx = new C_finalContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_c_final);
		try {
			setState(135);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FINAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(133);
				match(FINAL);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MetodoContext extends ParserRuleContext {
		public AlcanceContext alcance() {
			return getRuleContext(AlcanceContext.class,0);
		}
		public AbstractoContext abstracto() {
			return getRuleContext(AbstractoContext.class,0);
		}
		public PrototipoContext prototipo() {
			return getRuleContext(PrototipoContext.class,0);
		}
		public Valor_retornoContext valor_retorno() {
			return getRuleContext(Valor_retornoContext.class,0);
		}
		public EstaticoContext estatico() {
			return getRuleContext(EstaticoContext.class,0);
		}
		public C_finalContext c_final() {
			return getRuleContext(C_finalContext.class,0);
		}
		public MetodoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_metodo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterMetodo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitMetodo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitMetodo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MetodoContext metodo() throws RecognitionException {
		MetodoContext _localctx = new MetodoContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_metodo);
		try {
			setState(148);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(137);
				alcance();
				setState(138);
				abstracto();
				setState(139);
				prototipo();
				setState(140);
				valor_retorno();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(142);
				alcance();
				setState(143);
				estatico();
				setState(144);
				c_final();
				setState(145);
				prototipo();
				setState(146);
				valor_retorno();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbstractoContext extends ParserRuleContext {
		public TerminalNode LA() { return getToken(GeneradorCodigoParser.LA, 0); }
		public TerminalNode ABSTRACTA() { return getToken(GeneradorCodigoParser.ABSTRACTA, 0); }
		public TerminalNode LC() { return getToken(GeneradorCodigoParser.LC, 0); }
		public AbstractoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abstracto; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterAbstracto(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitAbstracto(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitAbstracto(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AbstractoContext abstracto() throws RecognitionException {
		AbstractoContext _localctx = new AbstractoContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_abstracto);
		try {
			setState(154);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LA:
				enterOuterAlt(_localctx, 1);
				{
				setState(150);
				match(LA);
				setState(151);
				match(ABSTRACTA);
				setState(152);
				match(LC);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Valor_retornoContext extends ParserRuleContext {
		public TerminalNode DOS_PUNTOS() { return getToken(GeneradorCodigoParser.DOS_PUNTOS, 0); }
		public Tipo_datoContext tipo_dato() {
			return getRuleContext(Tipo_datoContext.class,0);
		}
		public Valor_retornoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valor_retorno; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterValor_retorno(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitValor_retorno(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitValor_retorno(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Valor_retornoContext valor_retorno() throws RecognitionException {
		Valor_retornoContext _localctx = new Valor_retornoContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_valor_retorno);
		try {
			setState(159);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOS_PUNTOS:
				enterOuterAlt(_localctx, 1);
				{
				setState(156);
				match(DOS_PUNTOS);
				setState(157);
				tipo_dato();
				}
				break;
			case LC:
			case PUBLIC:
			case PRIVATE:
			case PROTECTED:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrototipoContext extends ParserRuleContext {
		public Nombre_funcionContext nombre_funcion() {
			return getRuleContext(Nombre_funcionContext.class,0);
		}
		public TerminalNode PA() { return getToken(GeneradorCodigoParser.PA, 0); }
		public ArgumentosContext argumentos() {
			return getRuleContext(ArgumentosContext.class,0);
		}
		public TerminalNode PC() { return getToken(GeneradorCodigoParser.PC, 0); }
		public PrototipoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prototipo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterPrototipo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitPrototipo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitPrototipo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrototipoContext prototipo() throws RecognitionException {
		PrototipoContext _localctx = new PrototipoContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_prototipo);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(161);
			nombre_funcion();
			setState(162);
			match(PA);
			setState(163);
			argumentos();
			setState(164);
			match(PC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Nombre_funcionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GeneradorCodigoParser.ID, 0); }
		public Nombre_funcionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nombre_funcion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterNombre_funcion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitNombre_funcion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitNombre_funcion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Nombre_funcionContext nombre_funcion() throws RecognitionException {
		Nombre_funcionContext _localctx = new Nombre_funcionContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_nombre_funcion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentosContext extends ParserRuleContext {
		public Tipo_datoContext tipo_dato() {
			return getRuleContext(Tipo_datoContext.class,0);
		}
		public TerminalNode ID() { return getToken(GeneradorCodigoParser.ID, 0); }
		public Varios_argumentosContext varios_argumentos() {
			return getRuleContext(Varios_argumentosContext.class,0);
		}
		public ArgumentosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argumentos; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterArgumentos(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitArgumentos(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitArgumentos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgumentosContext argumentos() throws RecognitionException {
		ArgumentosContext _localctx = new ArgumentosContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_argumentos);
		try {
			setState(173);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(168);
				tipo_dato();
				setState(169);
				match(ID);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(171);
				varios_argumentos();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Varios_argumentosContext extends ParserRuleContext {
		public Tipo_datoContext tipo_dato() {
			return getRuleContext(Tipo_datoContext.class,0);
		}
		public TerminalNode ID() { return getToken(GeneradorCodigoParser.ID, 0); }
		public TerminalNode COMA() { return getToken(GeneradorCodigoParser.COMA, 0); }
		public Varios_argumentosContext varios_argumentos() {
			return getRuleContext(Varios_argumentosContext.class,0);
		}
		public Varios_argumentosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varios_argumentos; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterVarios_argumentos(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitVarios_argumentos(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitVarios_argumentos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Varios_argumentosContext varios_argumentos() throws RecognitionException {
		Varios_argumentosContext _localctx = new Varios_argumentosContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_varios_argumentos);
		try {
			setState(183);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(175);
				tipo_dato();
				setState(176);
				match(ID);
				setState(177);
				match(COMA);
				setState(178);
				varios_argumentos();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(180);
				tipo_dato();
				setState(181);
				match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelacionContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(GeneradorCodigoParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(GeneradorCodigoParser.ID, i);
		}
		public FlechaContext flecha() {
			return getRuleContext(FlechaContext.class,0);
		}
		public ColoreadoContext coloreado() {
			return getRuleContext(ColoreadoContext.class,0);
		}
		public RelacionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relacion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterRelacion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitRelacion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitRelacion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelacionContext relacion() throws RecognitionException {
		RelacionContext _localctx = new RelacionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_relacion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			match(ID);
			setState(186);
			flecha();
			setState(187);
			match(ID);
			setState(188);
			coloreado();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FlechaContext extends ParserRuleContext {
		public TerminalNode HEREDA_DE() { return getToken(GeneradorCodigoParser.HEREDA_DE, 0); }
		public TerminalNode ES_PADRE_DE() { return getToken(GeneradorCodigoParser.ES_PADRE_DE, 0); }
		public TerminalNode DEPENDE_DE() { return getToken(GeneradorCodigoParser.DEPENDE_DE, 0); }
		public TerminalNode ES_USADA_POR() { return getToken(GeneradorCodigoParser.ES_USADA_POR, 0); }
		public TerminalNode IMPLEMENTA() { return getToken(GeneradorCodigoParser.IMPLEMENTA, 0); }
		public TerminalNode ES_IMPLEMENTADA_POR() { return getToken(GeneradorCodigoParser.ES_IMPLEMENTADA_POR, 0); }
		public TerminalNode SE_ASOCIA_A() { return getToken(GeneradorCodigoParser.SE_ASOCIA_A, 0); }
		public TerminalNode ES_ASOCIADO_CON() { return getToken(GeneradorCodigoParser.ES_ASOCIADO_CON, 0); }
		public TerminalNode ASOCIACION() { return getToken(GeneradorCodigoParser.ASOCIACION, 0); }
		public FlechaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_flecha; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).enterFlecha(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GeneradorCodigoListener ) ((GeneradorCodigoListener)listener).exitFlecha(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GeneradorCodigoVisitor ) return ((GeneradorCodigoVisitor<? extends T>)visitor).visitFlecha(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FlechaContext flecha() throws RecognitionException {
		FlechaContext _localctx = new FlechaContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_flecha);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << HEREDA_DE) | (1L << ES_PADRE_DE) | (1L << DEPENDE_DE) | (1L << ES_USADA_POR) | (1L << IMPLEMENTA) | (1L << ES_IMPLEMENTADA_POR) | (1L << ASOCIACION) | (1L << SE_ASOCIA_A) | (1L << ES_ASOCIADO_CON))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\39\u00c3\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3B\n\3"+
		"\3\4\3\4\3\4\3\4\3\4\5\4I\n\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5Q\n\5\3\6\3\6"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7`\n\7\3\b\3\b\5\bd\n\b"+
		"\3\t\3\t\3\n\3\n\3\n\3\n\3\n\5\nm\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\5\13v\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\17"+
		"\3\17\5\17\u0086\n\17\3\20\3\20\5\20\u008a\n\20\3\21\3\21\3\21\3\21\3"+
		"\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u0097\n\21\3\22\3\22\3\22\3\22"+
		"\5\22\u009d\n\22\3\23\3\23\3\23\5\23\u00a2\n\23\3\24\3\24\3\24\3\24\3"+
		"\24\3\25\3\25\3\26\3\26\3\26\3\26\3\26\5\26\u00b0\n\26\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\5\27\u00ba\n\27\3\30\3\30\3\30\3\30\3\30\3\31"+
		"\3\31\3\31\2\2\32\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\2"+
		"\6\6\2\f\f\16\17\37&\63\64\3\2\f\20\3\2\26\30\3\2)\61\2\u00bd\2\62\3\2"+
		"\2\2\4A\3\2\2\2\6H\3\2\2\2\bP\3\2\2\2\nR\3\2\2\2\f_\3\2\2\2\16c\3\2\2"+
		"\2\20e\3\2\2\2\22l\3\2\2\2\24u\3\2\2\2\26w\3\2\2\2\30}\3\2\2\2\32\177"+
		"\3\2\2\2\34\u0085\3\2\2\2\36\u0089\3\2\2\2 \u0096\3\2\2\2\"\u009c\3\2"+
		"\2\2$\u00a1\3\2\2\2&\u00a3\3\2\2\2(\u00a8\3\2\2\2*\u00af\3\2\2\2,\u00b9"+
		"\3\2\2\2.\u00bb\3\2\2\2\60\u00c0\3\2\2\2\62\63\7\66\2\2\63\64\5\4\3\2"+
		"\64\65\7\'\2\2\65\66\7\2\2\3\66\3\3\2\2\2\678\5\f\7\289\5\4\3\29B\3\2"+
		"\2\2:;\5.\30\2;<\5\4\3\2<B\3\2\2\2=>\5\6\4\2>?\5\4\3\2?B\3\2\2\2@B\3\2"+
		"\2\2A\67\3\2\2\2A:\3\2\2\2A=\3\2\2\2A@\3\2\2\2B\5\3\2\2\2CD\7\23\2\2D"+
		"I\5\n\6\2EF\7\24\2\2FI\5\n\6\2GI\5\b\5\2HC\3\2\2\2HE\3\2\2\2HG\3\2\2\2"+
		"I\7\3\2\2\2JK\7\25\2\2KL\7\63\2\2LQ\7\62\2\2MN\7\25\2\2NO\7\63\2\2OQ\7"+
		"\63\2\2PJ\3\2\2\2PM\3\2\2\2Q\t\3\2\2\2RS\t\2\2\2S\13\3\2\2\2TU\5\20\t"+
		"\2UV\7\63\2\2VW\7\64\2\2WX\5\16\b\2XY\5\22\n\2Y`\3\2\2\2Z[\5\20\t\2[\\"+
		"\7\63\2\2\\]\5\16\b\2]^\5\22\n\2^`\3\2\2\2_T\3\2\2\2_Z\3\2\2\2`\r\3\2"+
		"\2\2ad\7\65\2\2bd\3\2\2\2ca\3\2\2\2cb\3\2\2\2d\17\3\2\2\2ef\t\3\2\2f\21"+
		"\3\2\2\2gh\7\3\2\2hi\5\24\13\2ij\7\4\2\2jm\3\2\2\2km\3\2\2\2lg\3\2\2\2"+
		"lk\3\2\2\2m\23\3\2\2\2no\5\26\f\2op\5\24\13\2pv\3\2\2\2qr\5 \21\2rs\5"+
		"\24\13\2sv\3\2\2\2tv\3\2\2\2un\3\2\2\2uq\3\2\2\2ut\3\2\2\2v\25\3\2\2\2"+
		"wx\5\30\r\2xy\5\34\17\2yz\5\36\20\2z{\5\32\16\2{|\7\63\2\2|\27\3\2\2\2"+
		"}~\t\4\2\2~\31\3\2\2\2\177\u0080\7\63\2\2\u0080\33\3\2\2\2\u0081\u0082"+
		"\7\3\2\2\u0082\u0083\7\21\2\2\u0083\u0086\7\4\2\2\u0084\u0086\3\2\2\2"+
		"\u0085\u0081\3\2\2\2\u0085\u0084\3\2\2\2\u0086\35\3\2\2\2\u0087\u008a"+
		"\7\22\2\2\u0088\u008a\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u0088\3\2\2\2"+
		"\u008a\37\3\2\2\2\u008b\u008c\5\30\r\2\u008c\u008d\5\"\22\2\u008d\u008e"+
		"\5&\24\2\u008e\u008f\5$\23\2\u008f\u0097\3\2\2\2\u0090\u0091\5\30\r\2"+
		"\u0091\u0092\5\34\17\2\u0092\u0093\5\36\20\2\u0093\u0094\5&\24\2\u0094"+
		"\u0095\5$\23\2\u0095\u0097\3\2\2\2\u0096\u008b\3\2\2\2\u0096\u0090\3\2"+
		"\2\2\u0097!\3\2\2\2\u0098\u0099\7\3\2\2\u0099\u009a\7\20\2\2\u009a\u009d"+
		"\7\4\2\2\u009b\u009d\3\2\2\2\u009c\u0098\3\2\2\2\u009c\u009b\3\2\2\2\u009d"+
		"#\3\2\2\2\u009e\u009f\7\13\2\2\u009f\u00a2\5\32\16\2\u00a0\u00a2\3\2\2"+
		"\2\u00a1\u009e\3\2\2\2\u00a1\u00a0\3\2\2\2\u00a2%\3\2\2\2\u00a3\u00a4"+
		"\5(\25\2\u00a4\u00a5\7\5\2\2\u00a5\u00a6\5*\26\2\u00a6\u00a7\7\6\2\2\u00a7"+
		"\'\3\2\2\2\u00a8\u00a9\7\63\2\2\u00a9)\3\2\2\2\u00aa\u00ab\5\32\16\2\u00ab"+
		"\u00ac\7\63\2\2\u00ac\u00b0\3\2\2\2\u00ad\u00b0\5,\27\2\u00ae\u00b0\3"+
		"\2\2\2\u00af\u00aa\3\2\2\2\u00af\u00ad\3\2\2\2\u00af\u00ae\3\2\2\2\u00b0"+
		"+\3\2\2\2\u00b1\u00b2\5\32\16\2\u00b2\u00b3\7\63\2\2\u00b3\u00b4\7\7\2"+
		"\2\u00b4\u00b5\5,\27\2\u00b5\u00ba\3\2\2\2\u00b6\u00b7\5\32\16\2\u00b7"+
		"\u00b8\7\63\2\2\u00b8\u00ba\3\2\2\2\u00b9\u00b1\3\2\2\2\u00b9\u00b6\3"+
		"\2\2\2\u00ba-\3\2\2\2\u00bb\u00bc\7\63\2\2\u00bc\u00bd\5\60\31\2\u00bd"+
		"\u00be\7\63\2\2\u00be\u00bf\5\16\b\2\u00bf/\3\2\2\2\u00c0\u00c1\t\5\2"+
		"\2\u00c1\61\3\2\2\2\20AHP_clu\u0085\u0089\u0096\u009c\u00a1\u00af\u00b9";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}