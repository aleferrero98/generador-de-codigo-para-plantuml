import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

import javax.swing.JOptionPane;

import org.antlr.v4.runtime.tree.TerminalNode;

import tabla_simbolos.Argumento;
import tabla_simbolos.Atributo;
import tabla_simbolos.Identificador;
import tabla_simbolos.Metodo;
import tabla_simbolos.Miembro;
import tabla_simbolos.TablaSimbolos;

/**
 * Clase Escucha que implementa un listener. Recorre el arbol sintactico 
 * y va completando la Tabla de Simbolos. Realiza el analisis semantico y un chequeo de errores.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class Escucha extends GeneradorCodigoBaseListener {
    private TablaSimbolos tabla;
    private String error; // acumular errores
    Boolean flagErrores;
    private String warning; // acumula warnings
    private List<Miembro> atributos; // lista auxiliar que contiene atributos dentro de un bloque
    private List<Miembro> metodos; // lista auxiliar que contiene metodos dentro de un bloque
    private List<Argumento> argumentos; // lista auxiliar para los argumentos de un metodo
    private String nombreElemento; // guarda el nombre de la clase de la que se estan creando los metodos

    /**
     * Constructor que inicializa los strings de errores y warnings y la tabla de simbolos.
     * @param tabla referencia a la tabla de simbolos.
     */
    public Escucha(TablaSimbolos tabla) {
        this.tabla = tabla;
        this.error = "### COMPILACION REALIZADA SIN ÉXITO ### \n";
        this.flagErrores = false;
        this.warning = "";
    }
    /**
     * Establece que hay al menos un error y se le pasa el mensaje descriptivo
     * del error.
     * @param msj descripcion del error.
     */
    public void setErrores(String msj){
        this.flagErrores = true;
        this.error += msj + "\n";
    }

    /**
     * Al entrar a una regla declaracion se inicializan las listas de atributos y metodos
     * @param ctx contexto
     */
    @Override
    public void enterDeclaracion(GeneradorCodigoParser.DeclaracionContext ctx) {
        // por cada declaracion se crea una nueva lista de atributos y metodos
        this.atributos = new ArrayList<Miembro>();
        this.metodos = new ArrayList<Miembro>();
    }

    /**
     * Al salir de la regla declaracion se toma el tipo de elemento, nombre, metodos y 
     * atributos si posee y se agrega un nuevo Identificador con los datos extraidos a la Tabla de Simbolos.
     * Se chequea que no exista otro Identificador con el mismo nombre en la Tabla de Simbolos.
     * @param ctx contexto
     */
    @Override
    public void exitDeclaracion(GeneradorCodigoParser.DeclaracionContext ctx) {
        String tipo, id;
        tipo = ctx.tipo_elemento().getText();
        id = ctx.ID().getText();

        TerminalNode estereotipo = ctx.ESTEREOTIPO();
        //se verifica si el estereotipo es abstract para definir una clase abstracta
        if(estereotipo != null){
            if(estereotipo.getText().equals("<<abstract>>")){
                tipo = "abstract class";
            }
        }

        if (tabla.idExist(id)) {
            // ERROR
            setErrores("ERROR: ya existe un elemento con id=" + id);
            return;
        } else if (ctx.bloque().getText().equals("")) { // si no hay bloque, derivo en cadena vacia
            // si no hay bloque ni llaves si crea ID sin atributos ni metodos
            tabla.agregarID(new Identificador(id, tipo));
        } else if (ctx.bloque().contenido_bloque().getText().equals("")) { // hay bloque pero son solo las llaves
            // si el bloque esta vacio se crea ID sin atributos o metodos
            tabla.agregarID(new Identificador(id, tipo));
        } else {// el bloque posee atributos y/o metodos
            tabla.agregarID(new Identificador(id, tipo, this.atributos, this.metodos));
        }

    }
    /**
     * Al salir de la regla atributo se toman todos los datos del mismo y se añade un nuevo 
     * objeto Atributo a la lista de atributos.
     * @param ctx contexto
     */
    @Override
    public void exitAtributo(GeneradorCodigoParser.AtributoContext ctx) {
        String id = ctx.ID().getText();
        String tipoDato = ctx.tipo_dato().getText();
        String visibilidad = convertVisibilidad(ctx.alcance().getText());
        Boolean isStatic = (ctx.estatico().getText().equals("")) ? false : true;
        Boolean isFinal = (ctx.c_final().getText().equals("")) ? false : true;
        atributos.add(new Atributo(id, tipoDato, visibilidad, isStatic, isFinal));
    }

    /**
     * Convierte los simbolos de visibilidad del PlantUML a las palabras reservadas
     * public, private y protected del lenguaje Java.
     * 
     * @param alcance simbolos -, + o #
     * @return string public, private o protected
     */
    public String convertVisibilidad(String alcance) {
        if (alcance.equals("-")) {
            return "private";
        } else if (alcance.equals("+")) {
            return "public";
        } else if (alcance.equals("#")) {
            return "protected";
        } else {
            return null;
        }
    }
    /**
     * Inicializa la lista de argumentos al entrar a un metodo nuevo.
     * @param ctx contexto
     */
    @Override
    public void enterMetodo(GeneradorCodigoParser.MetodoContext ctx) {
        this.argumentos = new ArrayList<Argumento>();
    }
    /**
     * Al salir de la regla metodo, se obtienen todos los datos de un metodo y se lo añade a la lista de metodos.
     * @param ctx contexto
     */
    @Override
    public void exitMetodo(GeneradorCodigoParser.MetodoContext ctx) {
        String id = ctx.prototipo().nombre_funcion().getText(); //nombre de la funcion
        String tipoRet = ctx.valor_retorno().getText();
        String tipoDevuelto;
        String visibilidad = convertVisibilidad(ctx.alcance().getText());
        Boolean isAbstract = ctx.getChild(1).getText().equals("{abstract}");
        Boolean isStatic = ctx.getChild(1).getText().equals("{static}");
        Boolean isFinal = ctx.getChild(2).getText().equals("final");
        
        if(tipoRet.equals("") && !this.nombreElemento.equals(id)){
            tipoDevuelto = "void"; //es un metodo que no retorna valor
        }else if(this.nombreElemento.equals(id)){
            tipoDevuelto = ""; //es el Constructor de la clase
        }else{
            tipoDevuelto = ctx.valor_retorno().tipo_dato().getText(); //metodo que retorna valor
        }
        metodos.add(new Metodo(id, tipoDevuelto, visibilidad, isStatic, isFinal, isAbstract, this.argumentos));
    }
    /**
     * Al salir de la regla argumentos, se obtiene el id y tipo de dato del argumento y se agrega un nuevo
     * Argumento a la lista de argumentos.
     * @param ctx contexto
     */
    @Override
    public void exitArgumentos(GeneradorCodigoParser.ArgumentosContext ctx) {
        if (ctx.getText().equals("")) {// cadena vacia, no hay argumentos
            return;
        } else if (ctx.getChild(1) != null) {
            // si el 2do hijo no es null, entonces es un ID -> tenemos un solo argumento
            String id = ctx.getChild(1).getText();
            String tipo = ctx.getChild(0).getText();
            this.argumentos.add(new Argumento(id, tipo));
        }
    }
    /**
     * Para el caso de varios argumentos, se toma cada argumento (con su id y tipo) y se lo agrega a 
     * la lista de argumentos.
     * @param ctx contexto
     */
    @Override
    public void exitVarios_argumentos(GeneradorCodigoParser.Varios_argumentosContext ctx) {
        String id = ctx.ID().getText();
        String tipo = ctx.tipo_dato().getText();
        // se guardan siempre en la primera posicion y se desplaza el resto,
        // se hace asi para que se guarden en el orden dado.
        this.argumentos.add(0, new Argumento(id, tipo));
    }
    /**
     * Al salir de la regla relacion, se controlan errores semanticos entre los Identificadores y el 
     * vinculo que los relaciona. Tambien se verifica que dichos Identificadores hayan sido definidos previamente.
     * Para cada elemento que aparece, se setea su flag de usada.
     * @param ctx contexto
     */
    @Override
    public void exitRelacion(GeneradorCodigoParser.RelacionContext ctx) {
        Identificador idIzq, idDer;
        String nombreIzq, nombreDer;
        nombreIzq = ctx.ID(0).getText();
        nombreDer = ctx.ID(1).getText();
        // identificadores
        idIzq = tabla.getTablaSimbolos().get(nombreIzq);
        idDer = tabla.getTablaSimbolos().get(nombreDer);
        if (idIzq == null || idDer == null) {
            // ERROR
            setErrores("ERROR: elementos usados previo a su declaracion");
            return;
        }
        // nodos hojas
        TerminalNode hereda, esPadre, implementa, esImplementada;
        hereda = ctx.flecha().HEREDA_DE();
        esPadre = ctx.flecha().ES_PADRE_DE();
        implementa = ctx.flecha().IMPLEMENTA();
        esImplementada = ctx.flecha().ES_IMPLEMENTADA_POR();

        // el que sea distinto de null es porque ese es el nodo terminal
        if (hereda != null) { // --|>
            //en una herencia la superclase debe ser class o abstract class
            //y la clase hija debe ser class o abstract class
            if (idDer.getTipoElemento().equals("class") || idDer.getTipoElemento().equals("abstract class")) {
                if(idIzq.getTipoElemento().equals("class") || idIzq.getTipoElemento().equals("abstract class")){
                    idIzq.setClasePadre(idDer);
                    idIzq.setUsada(true);
                    idDer.setUsada(true);
                } else {
                    setErrores("ERROR: la clase hija no es de tipo <class> o <abstract class>");
                    return;
                }
            } else {
                setErrores("ERROR: la superclase no es de tipo <class> o <abstract class>");
                return;
            }
        } else if (esPadre != null) { // <|--
            if (idIzq.getTipoElemento().equals("class") || idIzq.getTipoElemento().equals("abstract class")) {
                if(idDer.getTipoElemento().equals("class") || idDer.getTipoElemento().equals("abstract class")){
                    idDer.setClasePadre(idIzq);
                    idDer.setUsada(true);
                    idIzq.setUsada(true);
                } else {
                    setErrores("ERROR: la clase hija no es de tipo <class> o <abstract class>");
                    return;
                }
            } else {
                setErrores("ERROR: la superclase no es de tipo <class> o <abstract class>");
                return;
            }
        } else if (implementa != null) { // ..|>
            if (idDer.getTipoElemento().equals("interface")) {
                idIzq.agregarInterfaz(idDer);
                idIzq.setUsada(true);
                idDer.setUsada(true);
            } else {
                setErrores("ERROR: la superinterfaz no es de tipo <interface>");
                return;
            }
        } else if (esImplementada != null) { // <|..
            if (idIzq.getTipoElemento().equals("interface")) {
                idDer.agregarInterfaz(idIzq);
                idDer.setUsada(true);
                idIzq.setUsada(true);
            } else {
                setErrores("ERROR: la superinterfaz no es de tipo <interface>");
                return;
            }
        } else {// se consideran las restantes flechas: Dependencia y Asociacion
            idDer.setUsada(true);
            idIzq.setUsada(true);
        }
    }

    /**
     * Al entrar a una regla bloque (definido por {..}) se guarda el nombre de la clase o interface que 
     * se este analizando.
     * @param ctx contexto
     */
    @Override
    public void enterBloque(GeneradorCodigoParser.BloqueContext ctx) {
        //guarda el nombre de la clase que se esta analizando, es para detectar el Constructor
        this.nombreElemento = ctx.getParent().getChild(1).getText();
    }

    /**
     * Verifica que no haya errores. En caso de que se presente al menos
     * uno, termina la ejecucion del programa.
     * Si son solo warnings, la ejecucion termina correctamente.
     * En ambos casos, se imprimen los errores y/o warnings en una ventana emergente.
     */
    public void checkErrores(){
        if(this.flagErrores){ //si hay errores, se imprimen y el programa finaliza sin exito
            System.out.print(this.error);
            System.out.println(this.warning);
            String msj = this.error + this.warning;
            JOptionPane.showMessageDialog(null, msj, "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }else{
            System.out.println(">>> COMPILACION REALIZADA EXITOSAMENTE <<<\n");
            System.out.println(this.warning);
            if(!this.warning.equals("")){
                JOptionPane.showMessageDialog(null, this.warning, "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, ">>> COMPILACION REALIZADA EXITOSAMENTE <<<", "RESULTADO COMPILACION", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }
    /**
     * Recorre todos los elementos de la tabla de simbolos y si hay algun
     * elemento que no haya sido usado, genera un warning.
     */
    public void esUsada(){
        int cant = 0;
        for(Identificador item : tabla.getTablaSimbolos().values()){
            if(!item.getUsada()){
                cant += 1;
            }
        }
        if(cant != 0){
            this.warning += "WARNING: Hay " + cant + " elementos inicializados que no se utilizan";
        }
    }
    
}
