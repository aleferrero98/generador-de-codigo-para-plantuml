grammar GeneradorCodigo;

//--------------------Analisis Lexico--------------------------
fragment DIGITO : [0-9]; 
fragment LETRA : [a-zA-Z];

LA : '{';
LC : '}';
PA : '(';
PC : ')';
COMA : ',';
GUION_BAJO : '_';
fragment ESPACIO : ' ';
fragment COMILLA_SIMPLE : ['];
COM_ANG_IZQ : '<<'; //comillas angulares
COM_ANG_DER : '>>';
DOS_PUNTOS : ':';

//Palabras reservadas
CLASE : 'class';
CLASE_ABSTRACTA : 'abstract class';
ENUM : 'enum';
INTERFACE : 'interface';
ABSTRACTA : 'abstract';
ESTATICO : 'static';
FINAL : 'final';
ESCONDER : 'hide';
MOSTRAR : 'show';
SKINPARAM : 'skinparam';
PUBLIC : '+';
PRIVATE : '-';
PROTECTED : '#';
IZQ : 'left';
DER : 'right';
ARRIBA : 'up';
ABAJO : 'down';
NOTA : 'note';
COMO : 'as';
MIEMBROS_VACIOS : 'empty members';
ATRIBUTOS_VACIOS : ('empty fields' | 'empty attributes');
METODOS_VACIOS : 'empty methods';
ATRIBUTOS : ('fields' | 'attributes');
METODOS : 'methods';
MIEMBROS : 'members'; //son metodos y atributos a la vez
CIRCULO : 'circle';
STEREOTYPE : 'stereotype';
END : '@enduml';
POSICION : IZQ | DER | ARRIBA | ABAJO;

//Relaciones
//Herencia
HEREDA_DE : ('--|>' | '-|>' | ('-'POSICION'-|>') | (POSICION'-|>'));
ES_PADRE_DE : ('<|--' | '<|-' | ('<|-'POSICION'-') | ('<|-'POSICION));
//Dependencia
DEPENDE_DE : ('..>' | '.>' | ('.'POSICION'.>') | (POSICION'.>'));
ES_USADA_POR : ('<..' | '<.' | ('<.'POSICION'.') | ('<.'POSICION));
//Interfaz
IMPLEMENTA : ('..|>' | '.|>' | ('.'POSICION'.|>') | (POSICION'.|>'));
ES_IMPLEMENTADA_POR : ('<|..' | '<|.' | ('<|.'POSICION'.') | ('<|.'POSICION));
//Asociación
ASOCIACION : ('--' | '-' | ('-'POSICION'-') | (POSICION'-'));
SE_ASOCIA_A : ('-->' | '->' | ('-'POSICION'->') | (POSICION'->'));
ES_ASOCIADO_CON : ('<--' | '<-' | ('<-'POSICION'-') | ('<-'POSICION));

NUMERO : DIGITO+;
ID : (LETRA | GUION_BAJO) (LETRA | DIGITO | GUION_BAJO)*;
ESTEREOTIPO : COM_ANG_IZQ ID COM_ANG_DER;
COLOR : '#'(DIGITO | LETRA)+;
//El START considera tambien el nombre que aparece despues de '@startuml'
START : '@startuml' .*? '\r'? '\n'; //toma todo lo que sigue del '@startuml' hasta el \n

WS : [ \n\t\r] -> skip;
COMENTARIO : COMILLA_SIMPLE .*? '\r'? '\n' -> skip;

//lineas horizontales que separan partes de la clase
SEPARADOR : ('--' | '..' | '==' | '__') -> skip;

//-------------------------Analisis Sintactico---------------------------
inicio : START diagrama_uml END EOF;

diagrama_uml : declaracion diagrama_uml
             | relacion diagrama_uml
             | modificadores diagrama_uml
             |
             ;

modificadores : ESCONDER objetos_a_esconder //hide
              | MOSTRAR objetos_a_esconder //show
              | comando_skinparam
              ;
//se puede poner la lista de todos los parametros de skinparam tambien
comando_skinparam : SKINPARAM ID NUMERO
                  | SKINPARAM ID ID
                  ;

objetos_a_esconder : MIEMBROS_VACIOS  //se listan los objetos a ser mostrados/escondidos
                   | ATRIBUTOS_VACIOS
                   | METODOS_VACIOS
                   | ATRIBUTOS
                   | METODOS
                   | MIEMBROS
                   | CIRCULO 
                   | STEREOTYPE
                   | CLASE
                   | INTERFACE
                   | ENUM
                   | ESTEREOTIPO
                   | ID
                   ;

declaracion : tipo_elemento ID ESTEREOTIPO coloreado bloque 
            | tipo_elemento ID coloreado bloque
            ;

coloreado : COLOR
          |
          ;

tipo_elemento : ENUM 
              | CLASE 
              | INTERFACE
              | CLASE_ABSTRACTA
              | ABSTRACTA
              ;

bloque : LA contenido_bloque LC
       |
       ;

contenido_bloque : atributo contenido_bloque
                 | metodo contenido_bloque
                 |
                 ;

//se permiten atributos static
atributo : alcance estatico c_final tipo_dato ID;

alcance : PUBLIC
        | PRIVATE
        | PROTECTED
        ;

tipo_dato : ID;

estatico : LA ESTATICO LC //para definir variables de clase
         |
         ;
//final para definir constantes en java para el caso de atributos
//un metodo final es aquel en donde las subclases no puedan sobreescribirlo.
c_final : FINAL 
        |
        ;

//se permite especificar metodos abstractos o estaticos
metodo : alcance abstracto prototipo valor_retorno
       | alcance estatico c_final prototipo valor_retorno 
       ;
//no se puede reimplementar los métodos estáticos (de clase) en las subclases. 
//Por lo tanto un método estático y abstracto no tiene sentido 
abstracto : LA ABSTRACTA LC 
          |
          ;
//regla que tiene en cuenta si el metodo devuelve un valor
valor_retorno : DOS_PUNTOS tipo_dato
              |
              ;

prototipo : nombre_funcion PA argumentos PC;

nombre_funcion : ID;

argumentos : tipo_dato ID  //1 argumento
           | varios_argumentos
           |
           ;

varios_argumentos : tipo_dato ID COMA varios_argumentos
                  | tipo_dato ID //ultimo argumento
                  ;

relacion : ID flecha ID coloreado; //es una relacion entre 2 clases

flecha : HEREDA_DE
       | ES_PADRE_DE
       | DEPENDE_DE
       | ES_USADA_POR
       | IMPLEMENTA
       | ES_IMPLEMENTADA_POR
       | SE_ASOCIA_A
       | ES_ASOCIADO_CON
       | ASOCIACION
       ;