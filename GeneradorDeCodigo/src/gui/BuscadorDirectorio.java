package gui;

import javax.swing.JTextField;
import javax.swing.JFileChooser;

/**
 * Objeto JDialog que abre un FileChooser para navegar entre archivos y directorios del sistema.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class BuscadorDirectorio extends javax.swing.JDialog {
    private JTextField text;

    /**
     * Constructor que inicializa los atributos y componentes del JDialog y lo hace visible.
     * Establece el JDialog como modal.
     * @param parent contenedor padre del JDialog, es el JFrame de la clase GUI
     * @param modal indica si se bloquea la ejecucion hasta que se cierre la ventana actual
     * @param file_dir indica si se debe buscar un archivo o un directorio
     * @param open_save indica si el boton de approve es de open o save.
     * @param text campo de texto que contendrá el path al directorio o archivo seleccionado.
     */
    public BuscadorDirectorio(java.awt.Frame parent, boolean modal, int file_dir, int open_save, JTextField text) {
        super(parent, modal);
        initComponents();
        this.text = text;
        this.FileChooser.setFileSelectionMode(file_dir);
        this.FileChooser.setDialogType(open_save);
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        FileChooser = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Navegador de archivos y directorios");
        setForeground(null);
        setIconImage(null);

        FileChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FileChooserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>                        

    /**
     * Realiza una accion cuando se da click al boton de approve o de cancelar.
     * Si es Cancelar se cierra la ventana actual.
     * Si es Approve (Open/Save) se toma el path al archivo o directorio seleccionado y se 
     * setea el campo de texto.
     * @param evt evento.
     */
    private void FileChooserActionPerformed(java.awt.event.ActionEvent evt) {                                            
        // TODO add your handling code here:
        if (evt.getActionCommand().equals(JFileChooser.APPROVE_SELECTION)) {
            this.text.setText(this.FileChooser.getSelectedFile().getAbsolutePath());
           // System.out.println(this.text.getText());
            this.dispose();
        } else if (evt.getActionCommand().equals(JFileChooser.CANCEL_SELECTION)) {
            this.dispose();
        }
    }                                           


    // Variables declaration - do not modify                     
    private javax.swing.JFileChooser FileChooser;
    // End of variables declaration                   
}
