import java.io.*;

/**
 * Clase Archivo que posee metodos para la lectura/escritura de archivos.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class Archivo{
   private String path; //Ruta en la cual se escribe/lee el archivo

   /**
    * Constructor que inicializa el path al archivo a leer/escribir.
    * @param path ruta al archivo a leer/escribir
    */
   public Archivo(String path){
      this.path = path;
   }
   /**
    * Devuelve el path
    * @return string con el path al archivo
    */
   public String getPath(){
      return this.path;
   }
   /**
    * Cambia el path al archivo
    * @param path ruta al archivo a leer/escribir
    */
   public void setPath(String path){
      this.path = path;
   }
   /**
    * Lee linea por linea el archivo especificado en el path y lo imprime en pantalla.
    */
   public void leerArchivo(){
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;

      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura linea por linea.
         archivo = new File (this.path);
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

         // Lectura del fichero linea por linea
         String linea;
         while((linea=br.readLine())!=null)
            System.out.println(linea); //se procesa una linea
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
   }

  /**
   * Escribe el string pasado por parámetro en el archivo indicado en path.
   * Si ya esta creado, lo sobreescribe.
   * @param contenido string a escribir en el archivo.
   */
   public void escribirArchivo(String contenido){
      FileWriter fichero = null;
      PrintWriter pw = null;
      try{
         //Para añadir al final del archivo, cambiar a true
         fichero = new FileWriter(this.path, false); //true si se quiere agregar al archivo existente
         pw = new PrintWriter(fichero);
         pw.print(contenido);
      }catch(Exception e){
         e.printStackTrace();
      }finally{
         try{
            if(null != fichero)
               fichero.close();
         }catch(Exception e2){
            e2.printStackTrace();
         }
      }
   }
}
