// Generated from /home/alejandro/Escritorio/FACULTAD/Practica_y_construccion_de_compiladores/TP_final/generador-de-codigo-para-plantuml/GeneradorDeCodigo/src/GeneradorCodigo.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GeneradorCodigoParser}.
 */
public interface GeneradorCodigoListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#inicio}.
	 * @param ctx the parse tree
	 */
	void enterInicio(GeneradorCodigoParser.InicioContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#inicio}.
	 * @param ctx the parse tree
	 */
	void exitInicio(GeneradorCodigoParser.InicioContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#diagrama_uml}.
	 * @param ctx the parse tree
	 */
	void enterDiagrama_uml(GeneradorCodigoParser.Diagrama_umlContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#diagrama_uml}.
	 * @param ctx the parse tree
	 */
	void exitDiagrama_uml(GeneradorCodigoParser.Diagrama_umlContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#modificadores}.
	 * @param ctx the parse tree
	 */
	void enterModificadores(GeneradorCodigoParser.ModificadoresContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#modificadores}.
	 * @param ctx the parse tree
	 */
	void exitModificadores(GeneradorCodigoParser.ModificadoresContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#comando_skinparam}.
	 * @param ctx the parse tree
	 */
	void enterComando_skinparam(GeneradorCodigoParser.Comando_skinparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#comando_skinparam}.
	 * @param ctx the parse tree
	 */
	void exitComando_skinparam(GeneradorCodigoParser.Comando_skinparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#objetos_a_esconder}.
	 * @param ctx the parse tree
	 */
	void enterObjetos_a_esconder(GeneradorCodigoParser.Objetos_a_esconderContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#objetos_a_esconder}.
	 * @param ctx the parse tree
	 */
	void exitObjetos_a_esconder(GeneradorCodigoParser.Objetos_a_esconderContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracion(GeneradorCodigoParser.DeclaracionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#declaracion}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracion(GeneradorCodigoParser.DeclaracionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#coloreado}.
	 * @param ctx the parse tree
	 */
	void enterColoreado(GeneradorCodigoParser.ColoreadoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#coloreado}.
	 * @param ctx the parse tree
	 */
	void exitColoreado(GeneradorCodigoParser.ColoreadoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#tipo_elemento}.
	 * @param ctx the parse tree
	 */
	void enterTipo_elemento(GeneradorCodigoParser.Tipo_elementoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#tipo_elemento}.
	 * @param ctx the parse tree
	 */
	void exitTipo_elemento(GeneradorCodigoParser.Tipo_elementoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#bloque}.
	 * @param ctx the parse tree
	 */
	void enterBloque(GeneradorCodigoParser.BloqueContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#bloque}.
	 * @param ctx the parse tree
	 */
	void exitBloque(GeneradorCodigoParser.BloqueContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#contenido_bloque}.
	 * @param ctx the parse tree
	 */
	void enterContenido_bloque(GeneradorCodigoParser.Contenido_bloqueContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#contenido_bloque}.
	 * @param ctx the parse tree
	 */
	void exitContenido_bloque(GeneradorCodigoParser.Contenido_bloqueContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#atributo}.
	 * @param ctx the parse tree
	 */
	void enterAtributo(GeneradorCodigoParser.AtributoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#atributo}.
	 * @param ctx the parse tree
	 */
	void exitAtributo(GeneradorCodigoParser.AtributoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#alcance}.
	 * @param ctx the parse tree
	 */
	void enterAlcance(GeneradorCodigoParser.AlcanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#alcance}.
	 * @param ctx the parse tree
	 */
	void exitAlcance(GeneradorCodigoParser.AlcanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#tipo_dato}.
	 * @param ctx the parse tree
	 */
	void enterTipo_dato(GeneradorCodigoParser.Tipo_datoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#tipo_dato}.
	 * @param ctx the parse tree
	 */
	void exitTipo_dato(GeneradorCodigoParser.Tipo_datoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#estatico}.
	 * @param ctx the parse tree
	 */
	void enterEstatico(GeneradorCodigoParser.EstaticoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#estatico}.
	 * @param ctx the parse tree
	 */
	void exitEstatico(GeneradorCodigoParser.EstaticoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#c_final}.
	 * @param ctx the parse tree
	 */
	void enterC_final(GeneradorCodigoParser.C_finalContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#c_final}.
	 * @param ctx the parse tree
	 */
	void exitC_final(GeneradorCodigoParser.C_finalContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#metodo}.
	 * @param ctx the parse tree
	 */
	void enterMetodo(GeneradorCodigoParser.MetodoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#metodo}.
	 * @param ctx the parse tree
	 */
	void exitMetodo(GeneradorCodigoParser.MetodoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#abstracto}.
	 * @param ctx the parse tree
	 */
	void enterAbstracto(GeneradorCodigoParser.AbstractoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#abstracto}.
	 * @param ctx the parse tree
	 */
	void exitAbstracto(GeneradorCodigoParser.AbstractoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#valor_retorno}.
	 * @param ctx the parse tree
	 */
	void enterValor_retorno(GeneradorCodigoParser.Valor_retornoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#valor_retorno}.
	 * @param ctx the parse tree
	 */
	void exitValor_retorno(GeneradorCodigoParser.Valor_retornoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#prototipo}.
	 * @param ctx the parse tree
	 */
	void enterPrototipo(GeneradorCodigoParser.PrototipoContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#prototipo}.
	 * @param ctx the parse tree
	 */
	void exitPrototipo(GeneradorCodigoParser.PrototipoContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#nombre_funcion}.
	 * @param ctx the parse tree
	 */
	void enterNombre_funcion(GeneradorCodigoParser.Nombre_funcionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#nombre_funcion}.
	 * @param ctx the parse tree
	 */
	void exitNombre_funcion(GeneradorCodigoParser.Nombre_funcionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#argumentos}.
	 * @param ctx the parse tree
	 */
	void enterArgumentos(GeneradorCodigoParser.ArgumentosContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#argumentos}.
	 * @param ctx the parse tree
	 */
	void exitArgumentos(GeneradorCodigoParser.ArgumentosContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#varios_argumentos}.
	 * @param ctx the parse tree
	 */
	void enterVarios_argumentos(GeneradorCodigoParser.Varios_argumentosContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#varios_argumentos}.
	 * @param ctx the parse tree
	 */
	void exitVarios_argumentos(GeneradorCodigoParser.Varios_argumentosContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#relacion}.
	 * @param ctx the parse tree
	 */
	void enterRelacion(GeneradorCodigoParser.RelacionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#relacion}.
	 * @param ctx the parse tree
	 */
	void exitRelacion(GeneradorCodigoParser.RelacionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GeneradorCodigoParser#flecha}.
	 * @param ctx the parse tree
	 */
	void enterFlecha(GeneradorCodigoParser.FlechaContext ctx);
	/**
	 * Exit a parse tree produced by {@link GeneradorCodigoParser#flecha}.
	 * @param ctx the parse tree
	 */
	void exitFlecha(GeneradorCodigoParser.FlechaContext ctx);
}