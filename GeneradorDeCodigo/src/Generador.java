import java.util.List;

import tabla_simbolos.Argumento;
import tabla_simbolos.Identificador;
import tabla_simbolos.Metodo;
import tabla_simbolos.Miembro;
import tabla_simbolos.TablaSimbolos;

/**
 * Clase Generador que recorre la tabla de simbolos y va generando 
 * los archivos vacios de las clases e interfaces en java, haciendo
 * uso de la clase Archivo.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class Generador {
    private TablaSimbolos tabla;
    private String directorio; //donde se guardaran los archivos generados

    /**
     * Constructor que inicializa la referencia a la Tabla de Simbolos y el path del directorio destino.
     * @param tabla tabla de simbolos
     * @param path ruta al directorio donde se guardaran los archivos generados.
     */
    public Generador(TablaSimbolos tabla, String path){
        this.tabla = tabla;
        this.directorio = path;
    }
    /**
     * Constructor que considera un directorio por defecto.
     * @param tabla tabla de simbolos
     */
    public Generador(TablaSimbolos tabla){
        this.tabla = tabla;
        this.directorio = "out/";
    }

    /**
     * Recorre la tabla de simbolos y va creando los archivos de clases,
     * interfaces y clases abstractas, con sus atributos y metodos.
     * Considera un elemento por archivo.
     */
    public void recorrerTabla(){
        Archivo file;
        String path, contenido;

        for(Identificador item : tabla.getTablaSimbolos().values()) {
            path = this.directorio + item.getID() + ".java";
            file = new Archivo(path);
            //prototipo clase/interfaz
            contenido = "public " + item.getTipoElemento() + " " + item.getID();
            contenido += ((item.getClasePadre() != null)? " extends "+item.getClasePadre().getID() : "");
            if(!item.getInterfaces().isEmpty()){
                int i;
                contenido += " implements ";
                for(i=0; i<item.getInterfaces().size()-1; i++){ //recorre hasta el anteultimo elemento
                    contenido += item.getInterfaces().get(i).getID() + ", ";
                }
                contenido += item.getInterfaces().get(i).getID(); //sin coma final
            }
            contenido += " {\n";
            
            //atributos
            if(!item.getAtributos().isEmpty()){
                contenido += "\t//Atributos\n";
                String estatico, cteFinal;
                for(Miembro atributo : item.getAtributos()){
                    estatico = (atributo.getIsStatic())? " static" : "";
                    cteFinal = (atributo.getIsFinal())? " final" : "";
                    contenido += "\t" + atributo.getVisibilidad() 
                              + estatico + cteFinal + " " + atributo.getTipo() 
                              + " " + atributo.getID() + ";\n";
                }
            }
            //metodos
            if(!item.getMetodos().isEmpty()){
                contenido += "\n";
                contenido += "\t//Metodos\n";
                String abstracto, estatico, cFinal, sep;
                for(Miembro metodo : item.getMetodos()) {
                    abstracto = (((Metodo) metodo).getIsAbstract()) ? " abstract" : "";
                    estatico = (metodo.getIsStatic())? " static" : "";
                    cFinal = (metodo.getIsFinal())? " final" : "";
                    sep = (metodo.getTipo().equals(""))? "" : " ";
                    contenido += "\t" + metodo.getVisibilidad() + abstracto
                              + estatico + cFinal + " " + metodo.getTipo()
                              + sep + metodo.getID() + "(";
                    
                    List<Argumento> argumentos = ((Metodo) metodo).getArgumentos();
                    if(!argumentos.isEmpty()){//se cargan los argumentos
                        int i;
                        for(i=0; i<argumentos.size()-1; i++){
                            contenido += argumentos.get(i).getTipo() + " " + argumentos.get(i).getID() + ", ";
                        }
                        contenido += argumentos.get(i).getTipo() + " " + argumentos.get(i).getID(); //ultimo arg sin coma final
                    }                  
                    contenido += ")";
                    if(((Metodo) metodo).getIsAbstract() || item.getTipoElemento().equals("interface")){
                    //si el metodo es abstracto o es una interfaz termina en ;
                        contenido += ";\n";
                    }else{
                        contenido += " {\n\t}\n";
                    }
                }
            }

            contenido += "}";
           // System.out.println(contenido);
            file.escribirArchivo(contenido);
        }

    }
    
}
