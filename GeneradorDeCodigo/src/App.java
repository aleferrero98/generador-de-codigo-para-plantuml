import java.util.concurrent.Semaphore;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import gui.GUI;
import tabla_simbolos.TablaSimbolos;

/**
 * Clase principal de la aplicación. Comienza la interfaz grafica, ejecuta el lexer, parser y listener.
 * En ausencia de errores, genera los archivos .java utilizando la clase Generador.
 * @author Alejandro Ferrero
 * @version 1.0.0
 */
public class App {
    public static void main(String[] args) throws Exception {
        
        Semaphore sem = new Semaphore(0); //semaforo para sincronizacion.
        GUI gui = new GUI(sem);
        gui.setVisible(true);
        try {
            sem.acquire(); //espera a que se de click al boton Generar codigo.
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        String pathUML = gui.getPathSrc();
        String pathDestino = gui.getPathDest();
        pathDestino += "/";
        
        System.out.println("Compilando " + pathUML + "...\n");
        //System.out.println(pathUML);
        //System.out.println(pathDestino);

        // create a CharStream that reads from file
        CharStream input = CharStreams.fromFileName(pathUML);

        // create a lexer that feeds off of input CharStream
        GeneradorCodigoLexer lexer = new GeneradorCodigoLexer(input);
        
        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        
        // create a parser that feeds off the tokens buffer
        GeneradorCodigoParser parser = new GeneradorCodigoParser(tokens);

        // create Listener
        GeneradorCodigoListener escucha = new Escucha(TablaSimbolos.getInstanceOf());
        
        // Conecto el objeto con Listeners al parser
        parser.addParseListener(escucha);
                
        // Solicito al parser que comience indicando una regla gramatical
        parser.inicio();
        ((Escucha) escucha).esUsada();
        ((Escucha) escucha).checkErrores();
        System.out.println(TablaSimbolos.getInstanceOf().toString());

        Generador generadorCodigo = new Generador(TablaSimbolos.getInstanceOf(), pathDestino); 
        generadorCodigo.recorrerTabla();
        
    }
}